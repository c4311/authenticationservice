const express = require('express');
const controller = require('./controllers/controller')
const middleware = require('./Middleware/crypto')

const routes=express();

routes.post('/signup',middleware.sign,controller.signup)
routes.post('/verify',middleware.verify,controller.verify)

module.exports = routes