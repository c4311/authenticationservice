const app = require('./app')


app.listen(process.env.LISTEN_PORT,process.env.LISTEN_HOST, () => {
    console.log(`AuthenticationService Running on port ${process.env.LISTEN_HOST}:${process.env.LISTEN_PORT} ...`);
  });