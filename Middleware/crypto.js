var jwt = require('jsonwebtoken');
require('dotenv').config()
var crypto = require('crypto');

var mycrypto = {}

mycrypto.sign = async function (req, res, next) {
    req.middleware = {}
    username = req.body.username
    tmp = username + process.env.API_KEY_SALT
    API_KEY = process.env.API_KEY_PREFIX + crypto.createHash('sha256').update(tmp).digest('hex');
    var token = jwt.sign({username: username, apikey: API_KEY}, process.env.JWT_SIGN_KEY);
    req.middleware.token = token
    req.middleware.apikey = API_KEY
    next()
}

mycrypto.verify = function (req, res, next) {
    let result = {};
    try {
        req.middleware = {}
        var decode = jwt.verify(req.body.token, process.env.JWT_SIGN_KEY)
        req.middleware.username = decode.username
        req.middleware.apikey = decode.apikey
        next()
    } catch (error) {
        result.data = [{verifed: false}];
        result.msg = 'error in verify jwt';
        res.status(200);
        res.send(JSON.stringify(result));
    }
}

module.exports = mycrypto