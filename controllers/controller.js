const {json} = require('body-parser')
const model = require('../models/model')
const msgs = require('./strings')


const controller = {}

controller.signup = async (req, res) => {
    let result = {}
    username = req.body.username
    apikey = req.middleware.apikey
    token = req.middleware.token
    try {
        tmp = await model.singup(username, apikey)
        if (tmp.success && tmp.rows) {
            result.data = [{token: token}]
            result.msg = msgs.msg_successful_en
            res.status(msgs.rc_Successful);
        } else {
            result.data = [{token: null}]
            result.msg = msgs.msg_errorDB_en
            res.status(msgs.rc_InternalServerError);
        }
    } catch (error) {
        result.data = [{token: null}]
        result.msg = msgs.msg_errorCONT_en
        res.status(msgs.rc_InternalServerError);
    }
    res.send(JSON.stringify(result))
}

controller.verify = async (req, res) => {
    let result = {}
    username = req.middleware.username
    apikey = req.middleware.apikey
    try {
        tmp = await model.verify(username, apikey)
        if (tmp.success && tmp.rows) {
            result.data = [{verifed: true}]
            result.msg = msgs.msg_successful_en
            res.status(msgs.rc_Successful);
        } else {
            result.data = [{verifed: false}]
            result.msg = msgs.msg_errorDB_en
            res.status(msgs.rc_InternalServerError);
        }
    } catch (error) {
        result.data = [{verifed: false}]
        result.msg = msgs.msg_errorCONT_en
        res.status(msgs.rc_InternalServerError);
    }
    res.send(JSON.stringify(result))
}


module.exports = controller