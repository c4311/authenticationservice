const pool =require('pg')
require('dotenv').config()



var poolins = new pool.Pool({
    user: process.env.DATABASE_USER,
    host: process.env.DATABASE_HOST,
    database: process.env.DATABASE,
    password: process.env.DATABASE_PASSWORD,
    port: process.env.DATABASE_PORT,
    ssl: process.env.DATABASE_SSL,
})

const model={}

model.singup=async function(username,APIkey){
    let result={}
    try {
        let tmp=await poolins.query('insert into users (username, "APIkey") values ($1,$2) returning "APIkey"',[username,APIkey]);
        result.success=true
        result.rows=tmp.rows
    } catch (error) {
        result.success=false
        result.error=error
    }
    return result
}

model.verify = async function(username,APIkey){
    let result={}
    try {
        let tmp=await poolins.query('select * from users where username=$1 and "APIkey"=$2',[username,APIkey]);
        result.success=true
        result.rows=tmp.rows
    } catch (error) {
        result.success=false
        result.error=error
    }
    return result
}

module.exports=model